package es.javaschool.spring_learn_project_autoconfigt.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import es.javaschool.calc.business_rules.MathUseCase;
import es.javaschool.calc.business_rules.impl.DiffUseCase;
import es.javaschool.calc.business_rules.impl.DivideUseCase;
import es.javaschool.calc.business_rules.impl.MultiplyUseCase;
import es.javaschool.calc.business_rules.impl.SumUseCase;
import es.javaschool.spring_learn_project_autoconfigt.props.ConfigProps;

@Configuration
@ComponentScan("es.javaschool.spring_learn_project_autoconfigt")
@EnableConfigurationProperties(ConfigProps.class)
public class Config {

	@Autowired
	private ConfigProps configProps;

	@Bean
	public MathUseCase<Integer, Integer, Integer> diff() {
		return new DiffUseCase();
	}

	@Bean
	public MathUseCase<Integer, Integer, Integer> sum() {
		return new SumUseCase();
	}

	@Bean
	public MathUseCase<Integer, Integer, Integer> mul() {
		return new MultiplyUseCase();
	}

	@Bean
	public MathUseCase<Integer, Integer, Integer> div() {
		return new DivideUseCase();
	}

}
