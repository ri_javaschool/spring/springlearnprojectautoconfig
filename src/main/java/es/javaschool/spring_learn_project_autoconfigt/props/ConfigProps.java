package es.javaschool.spring_learn_project_autoconfigt.props;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("es.math")
public class ConfigProps {

	private String testField;

	public String getTestField() {
		return testField;
	}

	public void setTestField(String testField) {
		this.testField = testField;
	}
}
