package es.javaschool.spring_learn_project_autoconfigt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLearnProjectAutoconfigtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringLearnProjectAutoconfigtApplication.class, args);
	}

}
