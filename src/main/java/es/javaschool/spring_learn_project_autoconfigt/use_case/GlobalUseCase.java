package es.javaschool.spring_learn_project_autoconfigt.use_case;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import es.javaschool.calc.business_rules.MathUseCase;

@Component
public class GlobalUseCase implements MathUseCase<Integer,Integer,Integer> {

	@Value("${hint:}")
	private String hint;


	@Override
	public Integer apply(Integer integer, Integer integer2) {
		return 42;
	}
}
